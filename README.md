# 基于CAS的简单应用演示
# 使用说明
启动cas,cas-app和cas-client
各应用入口

cas服务器登录地址:
https://sso.wsria.com:8443/cas/login

cas服务器退出地址:
https://sso.wsria.com:8443/cas/logout

cas-client登录:
http://127.0.0.1:8080/cas-client/login

cas-client退出:
http://127.0.0.1:8080/cas-client/logout

cas-app登录:
http://127.0.0.1:8080/cas-app/login

cas-app退出:
http://127.0.0.1:8080/cas-client/logout

登录用户名和密码:

使用salary数据库中users表的account作为用户名

使用salary数据库中users表的password作为密码

salary是数据库的库名,users是表名,数据库类型为mysql(详情配置参考cas\cas\WebContent\WEB-INF\deployerConfigContext.xml#120)

# 配置说明
# 环境
cas-server-4.0.0

cas-client-3.3.3

apache-tomcat-7.0.55

jdk1.7.0_67

MySql数据库,并执行以下脚本
```
CREATE SCHEMA `cas`;

USE `cas`;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `isDeleted` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0' COMMENT '是否删除.0:正常,1:删除',
  `account` varchar(255) NOT NULL COMMENT '帐号,该帐号唯一',
  `name` varchar(255) NOT NULL COMMENT '姓名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

INSERT INTO `users`(account, name, password) VALUES ('admin', '系统管理员', 'admin');
```

#CAS-Server服务端配置
1.域名映射

C:\Windows\System32\drivers\etc\hosts

添加内容如下：
```
127.0.0.1  sso.wsria.com
```
2.配置证书
可参考:http://www.kafeitu.me/sso/2010/11/05/sso-cas-full-course.html

证书生成步骤(证书和导出文件均放在项目根目录下keys目录中,使用可跳过正式生成步骤,直接导入)

生成证书:
```
keytool -genkey -alias wsria -keyalg RSA -keystore F:/keys/wsria.keystore
```

导出证书:
```
keytool -export -file F:/keys/wsria.crt -alias wsria -keystore F:/keys/wsria.keystore
```

3.Tomcat下server.xml添加内容
```
<!-- 设置证书 -->
<Connector SSLEnabled="true" URIEncoding="UTF-8" clientAuth="false" keystoreFile="F:/keys/wsria.keystore"
	keystorePass="123456" maxThreads="150" port="8443" protocol="HTTP/1.1"
	scheme="https" secure="true" sslProtocol="TLS"/>
```
>PS:若启动出现以下错误,把
>```
>protocol="HTTP/1.1" 更改为 >protocol="org.apache.coyote.http11.Http11Protocol"
>```
>错误信息:
>Setting property 'clientAuth' to 'false' did not find a matching property."

# CAS-Client客户端配置
1.JDK导入证书

备份证书库(备份之后要还原证书库只需要还原该文件即可)

F:\develop\jdk1.7.0_67\jre\lib\security\cacerts

导入生成的证书
```
keytool -import -keystore F:\develop\jdk1.7.0_67\jre\lib\security\cacerts -file F:/keys/wsria.crt -alias wsria
```
如果提示：
```
keytool error: java.io.IOException: Keystore was tampered with, or password was incorrect
```
那么请输入密码：`changeit`

证书导入错误恢复方法:
将备份好的

F:\develop\jdk1.7.0_67\jre\lib\security\cacerts

文件覆盖当前的同名文件即可

如果没有备份,可以在其他电脑上拷贝同版本JDK中的此文件

2.项目web.xml配置示例(以下是示例项目中的有关单点登录的配置,具体可以查看cas-app和cas-client中额web.xml,两个都配置一样)
```
<!-- ========================单点登录开始 ======================== -->
<!-- 用于单点退出，该过滤器用于实现单点登出功能，可选配置 -->
<listener>
	<listener-class>org.jasig.cas.client.session.SingleSignOutHttpSessionListener</listener-class>
</listener>
<!-- 该过滤器用于实现单点登出功能，可选配置。 -->
<filter>
	<filter-name>CAS Single Sign Out Filter</filter-name>
	<filter-class>org.jasig.cas.client.session.SingleSignOutFilter</filter-class>
	<init-param>
		<param-name>casServerUrlPrefix</param-name>
		<param-value>https://sso.wsria.com:8443/cas/</param-value>
	</init-param>
</filter>
<filter-mapping>
	<filter-name>CAS Single Sign Out Filter</filter-name>
	<url-pattern>/*</url-pattern>
</filter-mapping>

<!-- 该过滤器用于实现单点登录功能 -->
<filter>
	<filter-name>CAS Filter</filter-name>
	<filter-class>org.jasig.cas.client.authentication.AuthenticationFilter</filter-class>
	<init-param>
		<param-name>casServerLoginUrl</param-name>
		<param-value>https://sso.wsria.com:8443/cas/login</param-value>
		<!-- 使用的CAS-Server的登录地址,一定是到登录的action -->
	</init-param>
	<init-param>
		<param-name>serverName</param-name>
		<param-value>http://127.0.0.1:8080</param-value>
		<!-- 当前Client系统的地址 -->
	</init-param>
</filter>
<filter-mapping>
	<filter-name>CAS Filter</filter-name>
	<url-pattern>/*</url-pattern>
</filter-mapping>
<!-- 该过滤器负责对Ticket的校验工作 -->
<filter>
	<filter-name>CAS Validation Filter</filter-name>
	<filter-class>org.jasig.cas.client.validation.Cas20ProxyReceivingTicketValidationFilter</filter-class>
	<init-param>
		<param-name>casServerUrlPrefix</param-name>
		<param-value>https://sso.wsria.com:8443/cas/</param-value>
		<!-- 使用的CAS-Server的地址,一定是在浏览器输入该地址能正常打开CAS-Server的根地址 -->
	</init-param>
	<init-param>
		<param-name>serverName</param-name>
		<param-value>http://127.0.0.1:8080</param-value>
		<!-- 当前Client系统的地址 -->
	</init-param>
</filter>
<filter-mapping>
	<filter-name>CAS Validation Filter</filter-name>
	<url-pattern>/*</url-pattern>
</filter-mapping>

<!-- 该过滤器负责实现HttpServletRequest请求的包裹， 比如允许开发者通过HttpServletRequest的getRemoteUser()方法获得SSO登录用户的登录名，可选配置。 -->
<filter>
	<filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
	<filter-class>org.jasig.cas.client.util.HttpServletRequestWrapperFilter</filter-class>
</filter>
<filter-mapping>
	<filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
	<url-pattern>/*</url-pattern>
</filter-mapping>

<!--
	该过滤器使得开发者可以通过org.jasig.cas.client.util.AssertionHolder来获取用户的登录名。
	 比如AssertionHolder.getAssertion().getPrincipal().getName()
	 或者request.getUserPrincipal().getName()
-->
<filter>
	<filter-name>CAS Assertion Thread Local Filter</filter-name>
	<filter-class>org.jasig.cas.client.util.AssertionThreadLocalFilter</filter-class>
</filter>
<filter-mapping>
	<filter-name>CAS Assertion Thread Local Filter</filter-name>
	<url-pattern>/*</url-pattern>
</filter-mapping>
<!-- ========================单点登录结束 ======================== -->
```