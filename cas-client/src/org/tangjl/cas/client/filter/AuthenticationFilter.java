package org.tangjl.cas.client.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.tangjl.cas.client.servlet.LoginServlet;

public class AuthenticationFilter implements Filter {
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		String username = request.getRemoteUser();
		System.out.println("cas-client:" + username);

		HttpSession session = request.getSession(false);
		boolean isLogin = (null != session && null != session.getAttribute(LoginServlet.SESSION_USER));
		if (request.getRequestURI().indexOf("/login") != -1 || isLogin) {
			arg2.doFilter(arg0, arg1);
		} else {
			response.sendRedirect(request.getContextPath() + "/logout");
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
